package sunaryo.teddy.appx08

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    //inisiasi variabel
    lateinit var preferences : SharedPreferences
    val PREF_NAME = "setting"
    var BG_COLOR = "bg_color"
    var HDR_FONT_COLOR = "hdr_font_color"
    var SUBTITLE_FONT_SIZE = "title_font_size"
    var DETAIL_FONT_SIZE = "detail_font_size"
    var TITLE_TEXT = "title_text"
    var DETAIL_TEXT = "detail_text"
    val DEF_BG_COLOR = "GREY"
    val DEF_HDR_FONT_COLOR = "BLACK"
    val DEF_SUBTITLE_FONT_SIZE = 18
    val DEF_DETAIL_FONT_SIZE = 24
    val DEF_TITLE_TEXT = "Naruto Shippuden"
    val DEF_DETAIL_TEXT = "Naruto adalah sebuah serial manga karya Masashi Khishimoto yang diadaptasi menjadi serial anime. Mangan Naruto bercerita seputar kehidupan tokoh utamanya yaitu Naruto Uzumaki."

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onStart() {
        super.onStart()
        loadSetting()
    }

    fun loadSetting(){
        preferences = getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE)
        val DetailLayout = findViewById(R.id.detail) as ConstraintLayout
        DetailLayout.setBackgroundColor(Color.parseColor(preferences.getString(BG_COLOR,DEF_BG_COLOR)))
        txHeader.setTextColor(Color.parseColor(preferences.getString(HDR_FONT_COLOR,DEF_HDR_FONT_COLOR)))
        txSubTitle.textSize = preferences.getInt(SUBTITLE_FONT_SIZE,DEF_SUBTITLE_FONT_SIZE).toFloat()
        txDetail.textSize = preferences.getInt(DETAIL_FONT_SIZE,DEF_DETAIL_FONT_SIZE).toFloat()
        txTitle.setText(preferences.getString(TITLE_TEXT,DEF_TITLE_TEXT))
        txDetail.setText(preferences.getString(DETAIL_TEXT,DEF_DETAIL_TEXT))
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.option_menu,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.itemSetting ->{
                var intent = Intent(this,SettingActivity::class.java)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
