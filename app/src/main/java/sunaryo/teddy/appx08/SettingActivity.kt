package sunaryo.teddy.appx08

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.SeekBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_setting.*

class SettingActivity : AppCompatActivity(), View.OnClickListener, SeekBar.OnSeekBarChangeListener {

    //inisiasi variabel
    lateinit var preferences : SharedPreferences
    val PREF_NAME = "setting"
    var BG_COLOR = "bg_color"
    var HDR_FONT_COLOR = "hdr_font_color"
    var SUBTITLE_FONT_SIZE = "title_font_size"
    var DETAIL_FONT_SIZE = "detail_font_size"
    var TITLE_TEXT = "title_text"
    var DETAIL_TEXT = "detail_text"
    val DEF_BG_COLOR = "GREY"
    val DEF_HDR_FONT_COLOR = "BLACK"
    val DEF_SUBTITLE_FONT_SIZE = 18
    val DEF_DETAIL_FONT_SIZE = 24
    val DEF_TITLE_TEXT = "Naruto Shippuden"
    val DEF_DETAIL_TEXT = "Naruto adalah sebuah serial manga karya Masashi Khishimoto yang diadaptasi menjadi serial anime. Mangan Naruto bercerita seputar kehidupan tokoh utamanya yaitu Naruto Uzumaki."
    var FontHeaderColor : String = ""
    var BGColor : String = ""
    val arrayBGColor = arrayOf("BLUE","YELLOW","GREEN","BLACK")
    lateinit var adapterSpin : ArrayAdapter<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)
        //listener
        btnSimpan.setOnClickListener(this)
        rgBGHColor.setOnCheckedChangeListener { group, checkedId ->
            when(checkedId){
                R.id.rbBlue -> FontHeaderColor = "BLUE"
                R.id.rbYellow -> FontHeaderColor = "YELLOW"
                R.id.rbGreen -> FontHeaderColor = "GREEN"
                R.id.rbBlack -> FontHeaderColor = "BLACK"
            }
        }
        adapterSpin = ArrayAdapter(this,android.R.layout.simple_list_item_1,arrayBGColor)
        spBGColor.adapter = adapterSpin
        spBGColor.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                BGColor = adapterSpin.getItem(position).toString()
            }
        }
        //get resources from SharedPreferences
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        var spinnerselection = adapterSpin.getPosition(preferences.getString(BG_COLOR,DEF_BG_COLOR))
        spBGColor.setSelection(spinnerselection,true)
        var rgselection = preferences.getString(HDR_FONT_COLOR,DEF_HDR_FONT_COLOR)
        if (rgselection=="BLUE"){
            rgBGHColor.check(R.id.rbBlue)
        }else if(rgselection=="YELLOW"){
            rgBGHColor.check(R.id.rbYellow)
        }else if(rgselection=="GREEN"){
            rgBGHColor.check(R.id.rbGreen)
        }else{
            rgBGHColor.check(R.id.rbBlack)
        }
        sbFSub.progress = preferences.getInt(SUBTITLE_FONT_SIZE,DEF_SUBTITLE_FONT_SIZE)
        sbFSub.setOnSeekBarChangeListener(this)
        sbFDetail.progress = preferences.getInt(DETAIL_FONT_SIZE,DEF_DETAIL_FONT_SIZE)
        sbFDetail.setOnSeekBarChangeListener(this)
        edJudul.setText(preferences.getString(TITLE_TEXT,DEF_TITLE_TEXT))
        edDetail.setText(preferences.getString(DETAIL_TEXT,DEF_DETAIL_TEXT))
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnSimpan ->{
                //save configuration to SharedPreferences
                preferences = getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE)
                val prefEditor = preferences.edit()
                prefEditor.putString(BG_COLOR,BGColor)
                prefEditor.putString(HDR_FONT_COLOR,FontHeaderColor)
                prefEditor.putInt(SUBTITLE_FONT_SIZE,sbFSub.progress)
                prefEditor.putInt(DETAIL_FONT_SIZE,sbFDetail.progress)
                prefEditor.putString(TITLE_TEXT,edJudul.text.toString())
                prefEditor.putString(DETAIL_TEXT,edDetail.text.toString())
                prefEditor.commit()
                Toast.makeText(this,"Setting Disimpan",Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
        when(seekBar?.id){
            R.id.sbFSub ->{

            }
            R.id.sbFDetail ->{

            }
        }
    }

    override fun onStartTrackingTouch(seekBar: SeekBar?) {

    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {

    }

}